window._ = require('lodash');
window.$ = window.jQuery = require('jquery');
window.axios = require('axios');
window.auth_global = {
  id: '',
  first_name: '',
  last_name: '',
  email: '',
  username: '',
  type: '',
  isAuthenticated: false,
}

import Vue from 'vue'
import Vue2Filters from 'vue2-filters'
import Framework7 from 'framework7'
import Framework7Vue from 'framework7-vue'
import Framework7Icon from 'framework7-icons/css/framework7-icons.css'
import Framework7Theme from 'framework7/dist/css/framework7.ios.min.css'
import Framework7ThemeColors from 'framework7/dist/css/framework7.ios.colors.min.css'
import AppStyles from './assets/sass/main.scss'
import Routes from './routes.js'
import App from './main.vue'
import swal from 'sweetalert';

Vue.use(Vue2Filters)
Vue.use(Framework7Vue)

const app = new Vue({
  el: '#app',
  template: '<app/>',
  framework7: {
    root: '#app',
    routes: Routes
  },
  components: {
    app: App
  }
})
