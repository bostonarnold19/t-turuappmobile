export default [
  {
    path: '/login/',
    component: require('./assets/vue/pages/login.vue')
  },
  {
    path: '/registration/',
    component: require('./assets/vue/pages/registration.vue')
  },
  {
    path: '/schedule/',
    component: require('./assets/vue/pages/schedule.vue')
  },
  {
    path: '/profile/',
    component: require('./assets/vue/pages/profile.vue')
  },

  {
    path: '/teacher/add/subject',
    component: require('./assets/vue/teacher/add-subject.vue')
  },
  {
    path: '/teacher/edit/subject/id/:id',
    component: require('./assets/vue/teacher/edit-subject.vue')
  },
  {
    path: '/teacher/view/classroom/subject/:subjectCode/user_id/:id',
    component: require('./assets/vue/teacher/view-classroom.vue')
  },

  {
    path: '/forget/',
    component: require('./assets/vue/pages/forget.vue')
  },




]
